import { FileSystemItem } from './FileSystemItem'

export class FileSytemFile extends FileSystemItem {
  constructor(data) {
    super(data)
    this.file = ''
    Object.assign(this, data)
  }
}
