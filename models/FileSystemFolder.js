import { FileSystemItem } from './FileSystemItem'

export class FileSystemFolder extends FileSystemItem {
  constructor(data) {
    super(data)
    this.items = []
    Object.assign(this, data)
  }
}
