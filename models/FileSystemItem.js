export class FileSystemItem {
  constructor(data) {
    this.name = ''
    this.path = ''
    Object.assign(this, data)
  }
}
